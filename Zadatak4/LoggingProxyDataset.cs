﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak4
{
    class LoggingProxyDataset : IDataset
    {
        private Dataset dataset;
        private string filePath;
        private ConsoleLogger consoleLogger;

        public LoggingProxyDataset(string filePath)
        {
            this.filePath = filePath;
            consoleLogger = ConsoleLogger.GetInstance();  
        }

        public ReadOnlyCollection<List<string>> GetData()
        {
            consoleLogger.Log("Logged into "+filePath+" file: ");
            if (dataset == null)
            {
                dataset = new Dataset(filePath);
            }
            return dataset.GetData();
        }
    }
}
