﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak4
{
    class Program
    {
        static void Main(string[] args)
        {
            LoggingProxyDataset loggingDataset = new LoggingProxyDataset("sensitiveData.csv");
            DataConsolePrinter Consoleprinter = new DataConsolePrinter();
            Consoleprinter.PrintData(loggingDataset);
        }
    }
}
