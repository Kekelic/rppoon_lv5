﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak4
{
    class ConsoleLogger
    {
        private static ConsoleLogger instance;
        public string FilePath { get; set; }

        private ConsoleLogger()
        {
            this.FilePath = ("login.txt");
        }

        public static ConsoleLogger GetInstance()
        {
            if (instance == null)
            {
                instance = new ConsoleLogger();
            }
            return instance;
        }

        public void Log(string message)
        {
            using (System.IO.StreamWriter writer = new System.IO.StreamWriter(this.FilePath, true))
            {
                writer.WriteLine(message+DateTime.Now);
            }
        }
    }
}
