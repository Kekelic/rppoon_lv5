﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak7
{
    class Program
    {
        static void Main(string[] args)
        {
            ReminderNote note = new ReminderNote("I need to finish my laboratory exercises", new DarkTheme());

            GroupReminderNote groupReminderNote = 
                new GroupReminderNote("Tomorrow is the exam ", new LightTheme());
            groupReminderNote.Add("Stjepan");
            groupReminderNote.Add("Marko");

            List<string> students = new List<string>()
            {
                "Petar", "Ana", "Josip", "Ivana", "Martina", "Ivan"
            };
            GroupReminderNote groupNoteForStudents = new GroupReminderNote("Welcome to our faculty", new DarkTheme(), students);

            Notebook notebook = new Notebook(new DarkTheme());
            notebook.AddNote(note);
            notebook.AddNote(groupReminderNote);
            notebook.AddNote(groupNoteForStudents);
            notebook.Display();

            notebook.ChangeTheme(new LightTheme());
            notebook.Display();
        }
    }
}
