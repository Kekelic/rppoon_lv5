﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak7
{
    class GroupReminderNote : Note
    {
        private List<string> members;

        public GroupReminderNote(string message, ITheme theme) : base(message, theme)
        {
            this.members = new List<string>();
        }

        public GroupReminderNote(string message, ITheme theme, List<string> members) : base(message, theme)
        {
            this.members = members;
        }

        public void Add(string member)
        {
            members.Add(member);
        }

        public void Remove(string member)
        {
            members.Remove(member);
        }

        public override void Show()
        {
            this.ChangeColor();
            Console.Write("REMINDER FOR:");
            foreach (string member in members)
            {
                Console.Write(" " + member + ",");
            }
            Console.WriteLine();
            string framedMessage = this.GetFramedMessage();

            Console.WriteLine(framedMessage);
            Console.ResetColor();
        }
    }
}
