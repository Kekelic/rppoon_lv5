﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak3
{
    class DataConsolePrinter
    {
        public void PrintData(IDataset dataset)
        {
            ReadOnlyCollection<List<string>> data = dataset.GetData();
            if (data != null)
            {
                foreach (List<string> line in data)
                {
                    foreach (string word in line)
                    {
                        Console.Write(word + ' ');
                    }
                    Console.WriteLine();
                }
            }
            else
            {
                Console.Write("Can not access data!");
            }
        }
    }
}
