﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak3
{
    class Program
    {
        static void Main(string[] args)
        {
            VirtualProxyDataset VirtualDataset = new VirtualProxyDataset("sensitiveData.csv");
            DataConsolePrinter Consoleprinter = new DataConsolePrinter();  
            Consoleprinter.PrintData(VirtualDataset);
            Console.WriteLine();

            User user1 = User.GenerateUser("Stjepan");
            User user2 = User.GenerateUser("Marko");
            Consoleprinter.PrintData(new ProtectionProxyDataset(user1));
            Console.WriteLine();
            Consoleprinter.PrintData(new ProtectionProxyDataset(user2));
            Console.WriteLine();
                
        }
    }
}
