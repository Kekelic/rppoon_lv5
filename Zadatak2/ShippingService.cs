﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak2
{
    class ShippingService
    {
        private double pricePerUnitWeight;

        public ShippingService(double pricePerUnitWeight)
        {
            this.pricePerUnitWeight = pricePerUnitWeight;
        }

        public double PricePerUnitWeight { get { return pricePerUnitWeight; } set { pricePerUnitWeight = value; } }

        public double ShippingPrice(IShipable package)
        {
            return this.pricePerUnitWeight * package.Weight;
        }
    }
}
