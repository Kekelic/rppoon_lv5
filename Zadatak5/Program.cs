﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak5
{
    class Program
    {
        static void Main(string[] args)
        {
            ITheme theme = new DarkTheme();
            ReminderNote note = new ReminderNote("I need to finish my laboratory exercises", theme);
            note.Show();
        }
    }
}
