﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Zadatak6
{
    class Program
    {
        static void Main(string[] args)
        {
            GroupReminderNote groupReminderNote =
                new GroupReminderNote("Tomorrow is the exam ", new LightTheme());
            groupReminderNote.Add("Stjepan");
            groupReminderNote.Add("Marko");
            groupReminderNote.Show();

            List<string> students = new List<string>()
            {
                "Petar", "Ana", "Josip", "Ivana", "Martina", "Ivan"
            };
            GroupReminderNote groupNoteForStudents = new GroupReminderNote("Welcome to our faculty", new DarkTheme(), students);
            groupNoteForStudents.Show();
        }
    }
}
